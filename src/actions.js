
import strings from './locales'
const ITEMS_PER_PAGE = 10
import Toast from 'react-native-simple-toast';
import {
   SET_DEVICE
} from './constants'


export const setDevice = (device) => {
    return {
        type: SET_DEVICE,
        device
    }
}