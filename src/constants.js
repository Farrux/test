//we are using namespaceing to prevent module's action type collision
//every module should have a unique name. the best practice is to set name
//base on module's name

//name of this modules
export const NAME = 'app'


export const LOGIN = `${NAME}/LOGIN`
export const SET_USER = `${NAME}/SET_USER`
export const REGISTER = `${NAME}/REGISTER`
export const LOGOUT = `${NAME}/LOGOUT`


//as you can see above, each action is namespaced with module's name.