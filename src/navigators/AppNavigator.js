import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {
    addNavigationHelpers,
    StackNavigator,
} from 'react-navigation';

import MainComponent from '../components/MainComponent';
import RegisterComponent from '../components/RegisterComponent';

export const AppNavigator = StackNavigator({
    Main: {screen: MainComponent},
    Register: {screen: RegisterComponent}
}, {})
export const registerScreenAction =  AppNavigator.router.getActionForPathAndParams('Register')
const AppWithNavigationState = ({dispatch, nav}) => (
    <AppNavigator navigation={addNavigationHelpers({ dispatch, state: nav })}/>
);

AppWithNavigationState.propTypes = {
    dispatch: PropTypes.func.isRequired,
    nav: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    nav: state.nav,
});


export default connect(mapStateToProps)(AppWithNavigationState);