import {createStore, applyMiddleware, compose} from 'redux';
import {autoRehydrate} from 'redux-persist'
import rootReducer from "./reducers";
import {AsyncStorage} from 'react-native'
import {rootEpic} from './epic'
import {createEpicMiddleware} from "redux-observable";
const epicMiddleware = createEpicMiddleware(rootEpic);
const middleware = applyMiddleware(epicMiddleware);

export default () => {

    var store = createStore(rootReducer,
        undefined,
        compose(
            middleware,
        )
    )
    return store
}
