
import {
    SET_USER,
    LOGOUT
} from "../constants";
/**
 * Created by Farruxx on 21.05.2017.
 */


export default (state = {status:false}, action) => {
    switch (action.type) {
        case SET_USER:
        const {status} = action
            return {...state, ...{status: !state.status}}
        default:
            return state;
    }
}

