import {combineReducers} from 'redux';
import {NavigationActions} from 'react-navigation';
import {AppNavigator} from '../navigators/AppNavigator';
import login from "./login"
export const firstAction = AppNavigator.router.getActionForPathAndParams('Main');

const initialNavState = AppNavigator.router.getStateForAction(NavigationActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({
            routeName: 'Main',
        }),
    ],
}))

function nav(state = initialNavState, action) {
    let nextState;
    switch (action.type) {
        case 'pop':
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.back(),
                state
            );
            break;
        case 'Logout':
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate({routeName: 'Login'}),
                state
            );
            break;
        case 'error':
            var {error} = action
            if (error) {
                if (error.response && error.response.status == 401) {
                    nextState = AppNavigator.router.getStateForAction(
                        NavigationActions.navigate({routeName: 'Login'}),
                        state
                    );
                }
            }
            break
        case 'Main':
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate({routeName: 'Main'}),
                state
            );
            break;
       
        default:
            nextState = AppNavigator.router.getStateForAction(action, state);
            break;
    }

    // Simply return the original `state` if `nextState` is null or undefined.
    return nextState || state;
}


const AppReducer = combineReducers({
    nav,
	login
});

export default AppReducer;