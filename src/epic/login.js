import {

    SET_USER,
    LOGIN,
    REGISTER,

} from "../constants"
import * as Rx from "rxjs"

const onLogin = (data) => {
    return ({type: SET_USER, data,})
}

const onRegister = (data) => {
    return ({type: SET_USER, data,})
}

export const loginAction = () => {
    return ({type: LOGIN})
}
export const registerAction = () => {
    return ({type: REGISTER})
}
const fetchRx = (url, method, body, headers)=>
    Rx.Observable.timer(1000)
    .do(()=>console.debug(url, method, body, headers))
    .map(()=>'{"response":1}')

    export const loginEpic = (action$, store) =>
    action$.ofType(LOGIN)
        .flatMap(action =>
            fetchRx("login url", 'get', null, null)
                .takeUntil(action$.ofType(LOGIN))
                .flatMap(json => ([onLogin(json, true)]))
                .catch(error => Rx.Observable.of({type: 'error', error})))

export const registerEpic = (action$, store) =>
    action$.ofType(REGISTER)
        .flatMap(action =>
            fetchRx("register url", 'get', null, null)
                .takeUntil(action$.ofType(LOGIN))
                .flatMap(json => ([onRegister(json)]))
                .catch(error => Rx.Observable.of({type: 'error', error})))

