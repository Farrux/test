/**
 * Created by Farruxx on 18.06.2017.
 */
import {combineEpics} from 'redux-observable';
import {loginEpic, registerEpic} from './login'
export const rootEpic = combineEpics(
    loginEpic,
    registerEpic
)