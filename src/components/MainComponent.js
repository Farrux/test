/**
 * Created by Farruxx on 10.06.2017.
 */
import React, {Component} from 'react';
import {
    Text,
    TextInput,
    View,
    TouchableOpacity,
    StyleSheet,
    Platform
} from 'react-native';

import strings from '../locales'
import * as actions from '../actions'
import {connect} from "react-redux";
import moment from "moment"
import {loginAction} from '../epic/login'
import {registerScreenAction} from '../navigators/AppNavigator'

class MainComponent extends Component {
    static navigationOptions = {
        title: strings.app_name,
    };

    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        return <View style={{flex: 1, backgroundColor: "#2C4988",}}>
            <View>
                <Text style={styles.logo}>{strings.app_name}</Text>
                <Text style={{marginLeft: 15, color: 'white'}}>Login
                    status: {this.props.loginStatus ? "true" : "false"}</Text>
                <TextInput style={styles.input}
                           placeholder={strings.login}
                           onChangeText={(login) => this.setState({login})}
                           value={this.state.login}
                           editable={!this.props.loginStatus}
                           onSubmitEditing={() => {
                               this.refs['1'].focus()
                           }}
                           underlineColorAndroid='transparent'

                />

                <TextInput style={styles.input}
                           placeholder={strings.password}
                           secureTextEntry={true}
                           onChangeText={(password) => this.setState({password})}
                           value={this.state.password}
                           editable={!this.props.loginStatus}
                           ref='1'
                           underlineColorAndroid='transparent'
                />
                <TouchableOpacity
                    style={[styles.input, styles.submit]}
                    onPress={() => {
                        this.props.login()
                    }}>
                    <Text style={{color: 'white', fontSize: 16}}
                    >{this.props.loginStatus ? strings.logout : strings.submit}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={[styles.input, styles.submit, {backgroundColor: 'transparent'}]}
                    onPress={() => {
                        this.props.goRegister()
                    }}>
                    <Text style={{color: 'white', fontSize: 16}}
                    >{strings.register}</Text>
                </TouchableOpacity>
            </View>
        </View>
    }


}

const styles = StyleSheet.create({
    input: {
        height: 40,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 15,
        paddingLeft: 15,
        backgroundColor: "white"
    },
    submit: {
        backgroundColor: '#223B73',
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        fontSize: 35,
        color: 'white',
        alignSelf: 'center',
        marginTop: 100,
        marginBottom: 40,

    }
})

export default connect(
    (state) => ({
        loginStatus: state.login.status
    }),
    (dispatch) => ({
        login: () => dispatch(loginAction()),
        goRegister: () => dispatch(registerScreenAction)
    })
)(MainComponent)
