/**
 * Created by Farruxx on 10.06.2017.
 */
import React, {Component} from 'react';
import {
    Text,
    TextInput,
    View,
    TouchableOpacity,
    StyleSheet,
    Platform
} from 'react-native';

import strings from '../locales'
import * as actions from '../actions'
import {connect} from "react-redux";
import moment from "moment"
import {registerAction} from '../epic/login'

class RegisterComponent extends Component {
    static navigationOptions = {
        title: strings.register,
    };

    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        return <View style={{flex: 1, backgroundColor: "#2C4988",}}>
            <View>
                <Text style={styles.logo}>Welcome!</Text>
                <TextInput style={styles.input}
                           placeholder={strings.login}
                           onChangeText={(login) => this.setState({login})}
                           value={this.state.login}
                           editable={!this.props.loginStatus}
                           underlineColorAndroid='transparent'

                           onSubmitEditing={() => {
                               this.refs['1'].focus()
                           }}

                />

                <TextInput style={styles.input}
                           placeholder={strings.password}
                           secureTextEntry={true}
                           onChangeText={(password) => this.setState({password})}
                           value={this.state.password}
                           underlineColorAndroid='transparent'
                           ref='1'
                           onSubmitEditing={() => {
                               this.refs['2'].focus()
                           }}


                />
                <TextInput style={styles.input}
                           placeholder={strings.repassword}
                           secureTextEntry={true}
                           onChangeText={(repassword) => this.setState({repassword})}
                           value={this.state.repassword}
                           ref="2"
                           underlineColorAndroid='transparent'

                />
                <TouchableOpacity
                    style={[styles.input, styles.submit]}
                    onPress={() => {
                        this.props.register()
                    }}>
                    <Text style={{color: 'white', fontSize: 16}}
                    >{this.props.loginStatus ? strings.logout : strings.submit}</Text>
                </TouchableOpacity>

            </View>
        </View>
    }


}

const styles = StyleSheet.create({
    input: {
        height: 40,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 15,
        paddingLeft: 15,
        backgroundColor: "white"
    },
    submit: {
        backgroundColor: '#223B73',
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        fontSize: 35,
        color: 'white',
        alignSelf: 'center',
        marginBottom: 40,
        marginTop: 40
    }
})

export default connect(
    (state) => ({}),
    (dispatch) => ({
        register: () => dispatch(registerAction())
    })
)(RegisterComponent)
