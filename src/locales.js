/**
 * Created by Farruxx on 01.03.2017.
 */
import LocalizedStrings from 'react-native-localization';

const strings = new LocalizedStrings({
    en: {
        app_name: 'Login page demo',
        login: 'Login',
        name: 'Name',
        password:'Password',
        repassword:'Repeat password',
        submit:'Log In',
        logout:'Logout',
        register:'Registration'

    }
});

export default strings;