import React from 'react';
import {
    AsyncStorage,
    BackHandler,
    View,
    Image,
    Text
} from 'react-native'
import {Provider} from 'react-redux';
import createStore from './src/createStore';
import {persistStore} from 'redux-persist'
import AppReducer from './src/reducers';
import AppWithNavigationState from './src/navigators/AppNavigator'
const store = createStore(AppReducer);
import strings from './src/locales'
import * as actions from './src/actions'

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
		persistStore(store, {storage: AsyncStorage, blacklist: ['nav']}, () => {
            this.setState({rehydrated: true})
        })
        BackHandler.addEventListener('hardwareBackPress', () => {
            if (this.shouldCloseApp(store.getState().nav)) return false
            store.dispatch({
                type: 'Navigation/BACK'
            })
            return true
        })
   }
    render() {
        if (this.state.rehydrated) {
            return (
                <Provider store={store}>
                    <AppWithNavigationState />
                </Provider>
            )
        } else {
            return <View style={{flex:1,
            alignItems:'center',
			backgroundColor:'white',
            justifyContent:'flex-end'}}>
                <Text style={{color:'black', fontSize:20, marginBottom:20}}>....</Text>
            </View>
        }
    }

    shouldCloseApp(nav) {
        return nav.index == 0;
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('backPress')
    }
}

export default App;